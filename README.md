# IperBot
A side scroller developed on Unity.

[Click here for the web version!](http://dev.iperdesign.com/Iperbot/Ipergame_WebGL_Test/index.html)

## General info
IperBot is an internal project developed by several interns and designers. 
The game is currently availible on web but will soon be availible on iOS.

## Technologies
* Unity
* XCode

## Features
* 5 levels
* Browser support

## To-do list:
* Mobile support
* A updated web build

## Contributors
Eligio Sgaramella

Anthony Groves

Benny Feldman

Dante Massi

Konner Sidler

Thomas Ignarri

Katheryn Rodgers

John Merrill

Nayanika Vyas

Fernando Lopez

Armaan Bhasin

Erika Brown

developed at [Iperdeseign](https://www.iperdesign.com/)

## Contact
tom.ignarri@gmail.com
