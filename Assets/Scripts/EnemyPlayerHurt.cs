using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyPlayerHurt : MonoBehaviour 
{
	// Ammount of damage done to player on contact.
	public int damage = 1;

	// Reference to the player bot to deduct health upon collision.
	private GameObject currentPlayer;

    // Use this for initialization.
    void Start() 
    {
		currentPlayer = GameObject.FindGameObjectWithTag("Player");
	}

	void OnTriggerStay2D(Collider2D collision)
	{
        // If the player collides with the object, call the player object and the deduct health function.
        if (collision.gameObject.CompareTag("Player"))
        {
            currentPlayer.GetComponent<PlayerController>().playerDamaged(damage);
        }
        else if (collision.gameObject.CompareTag("Enemy"))
        {
            Debug.Log("The Enemies are touching themselves. " + gameObject.transform.name.ToString() + " On " + collision.gameObject.transform.name.ToString());
        }
    }
}
