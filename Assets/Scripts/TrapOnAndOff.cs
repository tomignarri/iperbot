﻿using System.Collections;
using System.Collections.Generic;
using System.Timers;
using UnityEngine;

public class TrapOnAndOff : MonoBehaviour 
{
	BoxCollider2D hitBox;

	public GameObject attachedTrap;
	public Animator anim;
    public AnimatorStateInfo stateInfo;

	public bool trapActive = false;

    public float timerstart;
	public float timer;

    // Not really the trap Delay, but changing the name would reset the stats of this variable space in all occurances.
	public float trapDelay = 8f;

    // Just displays clip time.
    public float timeOfAnimation;
    public bool laseron;

    public AudioClip laser;

    // Use this for initialization
    void Start() 
    {
		hitBox = gameObject.GetComponent<BoxCollider2D>();
        anim = attachedTrap.GetComponent<Animator>();
        timeOfAnimation = anim.runtimeAnimatorController.animationClips[0].length;

        stateInfo = anim.GetCurrentAnimatorStateInfo(0);

        // Trap stats Enabled or Disabled. Shouldn't be changed excet to correct error.
        hitBox.enabled = trapActive;
        timer = Time.time + timerstart;
	}

	// Update is called once per frame
	void Update() 
    {
        timeOfAnimation = anim.runtimeAnimatorController.animationClips[0].length;
        laseron = stateInfo.IsName("Base.LaserTrapOn");

        // Activate/Deactivate trap on set Interval
        if (Time.time > timer + trapDelay)
        {
            timer = Time.time;
            trapActive = !trapActive;
            anim.SetBool("Activated", trapActive);
            hitBox.enabled = trapActive;
        }
	}
}
