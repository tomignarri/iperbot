﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour 
{
    // An array of gameObjects to show upon a pause of the game
    GameObject[] pauseObjects;
    public Text pause;

	// Use this for initialization
    void Start() 
    {
        Time.timeScale = 1;
        AudioListener.pause = false;
        pauseObjects = GameObject.FindGameObjectsWithTag("ShowOnPause");
        hidePaused();
	}

    void Update() 
    {
        // Use P to pause and unpause
        if (Input.GetKeyDown(KeyCode.P))
        {
            pauseControl();
        }
	}

    // A function to reload the current level
    public void Reload() 
    {
      Destroy(GameObject.FindGameObjectWithTag("Respawn"));
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
  }

  public void Quit() 
  {
      Application.Quit();
  }

  // A function to control the time scale and pause menu upon clicking the pause button
  public void pauseControl() 
  {
      if (Time.timeScale == 1) 
      {
          AudioListener.pause = true;
          Time.timeScale = 0;
          showPaused();
      }
      else if (Time.timeScale == 0) 
      {
          AudioListener.pause = false;
          Time.timeScale = 1;
          hidePaused();
      }

#if UNITY_IOS || UNITY_ANDROID
        changePauseText(pause);
#endif
    }

    public void changePauseText(Text Pause) 
    {
        if (Pause.text == "III")
        {
            Pause.text = "X";
        }
        else
        {
            Pause.text = "III";
        }
    }

    // Iterating through an array of tagged items to appear upon pausing
    public void showPaused() 
    {
      foreach (GameObject g in pauseObjects) 
      {
          g.SetActive(true);
      }
  }

  // Iterating through an array of tagged items to appear upon unpausing
  public void hidePaused() 
  {
      foreach (GameObject g in pauseObjects) 
      {
          g.SetActive(false);
      }
  }

  // A function that returns game to main menu screen
  public void LoadMain() 
  {
        if (Time.timeScale == 0) 
        {
          AudioListener.pause = false;
          Time.timeScale = 1;
          hidePaused();
          }

        Destroy(GameObject.FindGameObjectWithTag("Respawn"));
        Destroy(GameObject.FindGameObjectWithTag("Score"));
        SceneManager.LoadScene("Scenes/MainMenu");
  }
}
