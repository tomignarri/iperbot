﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenuButtonManager : MonoBehaviour 
{
    // Load Scene Based on Button
    public void NewGameBtn(string newGameLevel) 
    {
        SceneManager.LoadScene("Scenes/" + newGameLevel);
    }

    // Quit the Game (Used for PC/Mac/Linux Build)
	public void Quit()
	{
        Application.Quit();
	}
}
