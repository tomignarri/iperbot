﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthCounterImageManager : MonoBehaviour
{
    // A count for how many health remains.
    private int count;
    private float percent;

    private GameObject player;

    public Image Healthbar;
    public Sprite Health4;
    public Sprite Health3;
    public Sprite Health2;
    public Sprite Health1;

    // Use this for initialization.
    void Start()
    {
        // Start with the first count to text and set the text
        player = GameObject.FindGameObjectWithTag("Player");
        count = player.GetComponent<PlayerController>().healthRemaining;
        Healthbar.sprite = Health4;
    }

    void Update()
    {
        // Monitor health constantly to detect changes
        count = player.GetComponent<PlayerController>().healthRemaining;
        SetNewImage();
    }

    public void SetNewImage()
    {
        // Update the text to match the new count
        switch (count)
        {
            default:
            case 4:
                Healthbar.sprite = Health4;
                break;
            case 3:
                Healthbar.sprite = Health3;
                break;
            case 2:
                Healthbar.sprite = Health2;
                break;
            case 1:
                Healthbar.sprite = Health1;
                break;
            case 0:
                Healthbar.rectTransform.sizeDelta = new Vector2(0, 0);
                break;
        }
    }
}
