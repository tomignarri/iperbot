﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelChangeTrigger : MonoBehaviour 
{
    // Level you would like to change to upon trigger
    public string selectedLevel = "MainMenu";

    public bool rightHitBox = true;

    public GameObject teleporter;

    private Animator levelChangeAnim;

    public float changeDelay = 3f;

    void Start()
    {
        levelChangeAnim = teleporter.GetComponent<Animator>();
    }

    /*void OnTriggerEnter2D(Collider2D collision)
    {
        // Check to see if the collider is from the Player model.  If so, change to desired level
        if ((collision.gameObject.CompareTag("Player"))) {
            playerAnim.SetBool("Off", true);
            //IMPORTANT MAKE SURE LEVEL CHANGES AFTER OINTS ARE TALLIED
            Invoke("LoadNewScene", changeDelay);
            if (rightHitBox)
                levelChangeAnim.SetBool("EndOfLevelRight", true); //anim triggers
            else
                levelChangeAnim.SetBool("EndOfLevelLeft", true); //anim triggers
        }

        PlayerController player = GameObject.FindGameObjectWithTag("Player").GetComponent<PlayerController>();
        player.EndLevelLock();
        player.canBeHit = false;
    }*/

    public IEnumerator animateTeleport()
    {
        if (rightHitBox)
        {
            gameObject.transform.parent.GetComponent<SpriteRenderer>().flipX = true;
        }

        levelChangeAnim.SetBool("EndOfLevel", true); 
        yield return new WaitUntil(() => levelChangeAnim.GetCurrentAnimatorStateInfo(0).IsName("EndOfLevelOn"));
        Debug.Log("Animation's done");
    }
}