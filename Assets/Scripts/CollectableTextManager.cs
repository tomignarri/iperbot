﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableTextManager : MonoBehaviour {

	// A count for how many collectables are collected
	public int count = 0;

	public Text CollectableText;

	// Use this for initialization
	void Start() {
		// Start with the first count to text and set the text
		CollectableText.text = "Collectables: " + count.ToString();
	}

	public void SetNewText() {
		// Add one to the count of collectables
		count = count + 1;
        Debug.Log("COUNT IS ON TEXT " + count);

        // Update the text to match the new count
        CollectableText.text = "Collectables: " + count.ToString();
	}
}
