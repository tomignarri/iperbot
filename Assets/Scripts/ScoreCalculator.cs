﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ScoreCalculator : MonoBehaviour 
{
    private float startTime;
    private int numDeaths = 0;
    private int numHits = 0;
    private List<string> collectablessaved = new List<string>();
    TotalScoreHolder Total;

    // Use this for initialization.
    void Start()
    {
        startTime = Time.time;
        Total = GameObject.FindGameObjectWithTag("Score").GetComponent<TotalScoreHolder>();
    }

    // Method to calculate the points from this level, send it the TotalPointHolder, and destroy this gameObject.
    public IEnumerator tallyPoints(int health, float delayTime)
    {
        if (Total == null)
        {
            yield break;
        }

        Total.addCompletion();
        Total.countCollectables(collectablessaved.Count);
        yield return new WaitForSeconds(delayTime);
        int timeTaken = (int) (Time.time - startTime);
        Total.countTime(timeTaken);
        Total.countDeaths(numDeaths);
        Total.countHits(numHits);
    }

    public void addDeath()
    {
        numDeaths += 1;
    }

    public void addHit()
    {
        numHits += 1;
    }

    public void setSaved(List<string> collected)
    {
        collectablessaved = new List<string>(collected);
        Debug.Log(collectablessaved);
    }

    public List<string> getCollectables()
    {
        return collectablessaved;
    }
}