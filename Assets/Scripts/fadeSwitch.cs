﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fadeSwitch : MonoBehaviour 
{
    // An array to store which platforms will be turned off upon stepping on this platform.
    [SerializeField]
    private GameObject[] offPlatforms;

    // An array to store which platforms will be made active upon stepping on this platform.
    [SerializeField]
    private GameObject[] onPlatforms;

    private bool collided;

    // player colliding with switch.
    private void OnTriggerEnter2D(Collider2D other) 
    {
        if (other.gameObject.CompareTag("Player"))
        {
            this.Switch();
            this.collided = true;
        }
    }

    // Goes through the arrays to turn platforms on and off.
    private void Switch() 
    {
                // Platforms being turned off.
                foreach (GameObject p in this.offPlatforms)
                {
                    if (p.GetComponent<FadeOutIn>().inProgress == false) 
                    {
                        p.GetComponent<FadeOutIn>().OffSwitch();
                        p.GetComponent<FadeOutIn>().TurnPlats();
                    }
                }

                // Platforms being activated.
                foreach (GameObject p in this.onPlatforms)
                {
                    if (p.GetComponent<FadeOutIn>().inProgress == false) 
                    {
                            p.GetComponent<FadeOutIn>().OnSwitch();
                            p.GetComponent<FadeOutIn>().TurnPlats();
                    }
              }
        }
}
