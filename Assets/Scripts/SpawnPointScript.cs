﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SpawnPointScript : MonoBehaviour 
{
	// Use this for initialization
    private int spawnhealth = 4;

    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Debug.Log("Deleting extraneous copy");
            gameObject.transform.position = new Vector2(0, 0);
            Destroy(gameObject);
        }
        else 
        {
            Debug.Log("Awake: " + this.gameObject);
        }
    }

    public void setSpawnHealth(int health)
    {
        ////spawnhealth = health;
    }

    public int getSpawnHealth()
    {
        return spawnhealth;
    }
}
