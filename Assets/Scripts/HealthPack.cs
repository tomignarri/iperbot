﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HealthPack : MonoBehaviour 
{
    // Health to Restore.
	public int strength = 1;

	void OnTriggerEnter2D(Collider2D collision)
	{
		// If the player collides with the object, increase health.
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.GetComponent<PlayerController>().addHealth(strength);
        }
    }
}
