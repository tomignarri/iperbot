﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FadeOutIn : MonoBehaviour 
{
    [HideInInspector]
    public bool inProgress = false;

    // How long you have till platform fades completely.
    [SerializeField]
    private float fadeTime = 0.5f;

    // Transparency of platform in inactive state.
    [SerializeField]
    private float fadeamount = 0.0f;

    // Is the platform active or not active?
    [SerializeField]
    private bool platformActive = true;

    // Make platforms visible or invisible based on current state
	public void TurnPlats()
    {
		if (!this.inProgress) 
        {
            if (this.platformActive)
            {
                StartCoroutine(this.FadeTo(1.0f, this.fadeTime));
            }
            else
            {
                StartCoroutine(this.FadeTo(this.fadeamount, this.fadeTime));
            }
         }
    }

    private void Start()
    {
        this.TurnPlats();
    }

    private IEnumerator FadeTo(float aValue, float aTime)
	{
		// Boolean to lock the change state until completion
		this.inProgress = true;
        
		// Yield return new WaitForSeconds(fadeDelayTime);
		float alpha = transform.GetComponent<SpriteRenderer>().material.color.a;
		for (float t = 0.0f; t < 1.0f; t += Time.deltaTime / aTime)
		{
				Color newColor = new Color(1, 1, 1, Mathf.Lerp(alpha, aValue, t));
				transform.GetComponent<SpriteRenderer>().material.color = newColor;
				yield return null;
		}

        // To make sure it ends either completly transparent or completely opaque.
        transform.GetComponent<SpriteRenderer>().material.color = new Color(1, 1, 1, aValue);

        Component[] boxes;

        boxes = gameObject.GetComponents<BoxCollider2D>();

        if (aValue == this.fadeamount) 
        {
			// Iterates through an array of colliders to switch them off
			foreach (BoxCollider2D b in boxes) 
            {
				b.enabled = false;
			}
		} 
        else 
        {
			// Iterates through an array of colliders to switch them on
			foreach (BoxCollider2D b in boxes) 
            {
				b.enabled = true;
			}
		}

		this.inProgress = false;
	}

	// Switches a boolean to turn a platform to the off state
	public void OffSwitch() 
    {
		this.platformActive = false;
	}

	// Switches a boolean to turn a plaform to the active state
	public void OnSwitch() 
    {
		this.platformActive = true;
	}
}
