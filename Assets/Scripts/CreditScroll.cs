﻿using UnityEngine;
using System.Collections;

public class CreditScroll : MonoBehaviour
{
    public float scrollSpeed;
    public float endposition;

    private Vector2 startPosition;
    // Use this for initialization
    void Start()
    {
        startPosition = transform.position;
    }


    void Update()
    {
        if (gameObject.GetComponent<RectTransform>().anchoredPosition.y != endposition)
        {
            float moveamount = Time.deltaTime * scrollSpeed;
            if (gameObject.GetComponent<RectTransform>().anchoredPosition.y + moveamount < endposition)
                transform.position = new Vector2(transform.position.x, transform.position.y + moveamount);

            else
                gameObject.GetComponent<RectTransform>().anchoredPosition = new Vector2(gameObject.GetComponent<RectTransform>().anchoredPosition.x, endposition);
        }
    }
}
