﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BlockCrawlerEnemyController : MonoBehaviour 
{
    // An array to store transforms of movement points for the enemy.
    [SerializeField]
    private Transform[] movePoints;

    // A counter to iterate each point in the movePoints array.
    private int cur = 0;

    // Speed of enemy movements.
    [SerializeField]
    private float speed = 0.5f;

    // Boolean for flipping animations.
    [SerializeField]
    private bool clockwise = true;

    [SerializeField]
    private AudioSource crawlingNoise;

    private void Awake()
    {
        this.crawlingNoise = GetComponent<AudioSource>();
    }

    // Called upon every physics point.
    private void FixedUpdate()
    {
        // Check to see if the enemy is not at the next point in the array.
        if (transform.position != this.movePoints[this.cur].position)
            {
            // If the enemy is not at the cur position, move it towards that position.
            Vector2 p = Vector2.MoveTowards(transform.position, this.movePoints[this.cur].position, speed);

            // If the enemy reaches a point in the array, instance the next point.
            GetComponent<Rigidbody2D>().MovePosition(p);
            }
                    else
                    {
						this.cur = (this.cur + 1) % movePoints.Length;
                        if (clockwise)
                        {
                            transform.Rotate(0, 0, -90);
                        } 
                        else 
                        {
                            transform.Rotate(0, 0, 90);
                        }
                    }
      }     
}
