﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathTrigger : MonoBehaviour 
{

	void OnTriggerEnter2D(Collider2D collision)
	{
		// If the player collides with the object, restart the level
        if (collision.gameObject.CompareTag("Player")) 
        {
            if (!collision.gameObject.GetComponent<PlayerController>().death) 
            {
                collision.GetComponent<PlayerController>().canBeHit = true;
                collision.GetComponent<PlayerController>().playerDamaged(999);
            }
        }
	}

	void Respawn() 
    {
		SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
	}
}
