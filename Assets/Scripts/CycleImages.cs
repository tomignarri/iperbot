﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CycleImages : MonoBehaviour
{
    public Image Display;
    public Sprite[] sprites;
    private int index = 0;
    void Start()
    {
        Display.sprite = sprites[index];
    }

    public void ChangeImage(int direction)
    {
        index += direction;

        if (index >= sprites.Length)
        {
            index = sprites.Length - 1;
        }

        else if (index < 0)
        {
            index = 0;
        }

        else
        {
            Display.sprite = sprites[index];
        }
    }

    // Update is called once per frame
}
