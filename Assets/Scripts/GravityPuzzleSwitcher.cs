﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GravityPuzzleSwitcher : MonoBehaviour 
{
	private GameObject player;

	// A boolean to allow a switch to be used only once.
	public bool used = false;
	public bool switchedOn = false;
	public bool OnceOnly = false;

	private Animator anim;

    public AudioClip gravityFlip;

    // Use this for initialization.
    void Start () 
    {
		anim = GetComponent<Animator>();
		player = GameObject.FindGameObjectWithTag("Player");
	}

	void Update() 
    {
		anim.SetBool("Activated", used);
	}

	// If player touches unused switch, swap gravity.
	void OnTriggerEnter2D(Collider2D collider) 
    {
		if (collider.gameObject.CompareTag("Player") && !used) 
        {
            GetComponent<AudioSource>().PlayOneShot(gravityFlip);
            used = !used;
			PlayerController PC = player.GetComponent<PlayerController>();
			PC.gravityPuzzle = true;

            // Delayed called call.
            Invoke("multiUseTimer", 10f); 
		}
	}

    // Switch gravity switch to off postiton again if not a once only switch.
	void multiUseTimer() 
    {
		if (!OnceOnly)
        {
            used = false;
        }
	}
}
