﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlatformMover : MonoBehaviour 
{
    // An array to store transforms of movement points for platforms
    public Transform[] movePoints;

    // A boolean to set the platforms to move. Called in PlatformAddMovepointTrigger
    public bool activated = false;

    // A counter to iterate each point in the movePoints array
    int cur = 0;

    // Speed of platform movements.
    public float speed = 3.00f;

    // Test to see if getting x velocity si really this easy.
    public float xspeed = 0;
    public float cosine = 0;
    public bool squishing = false;
    public Vector2 A;
    public Vector2 B;

    private Vector2 limit;
    private float angle;

    void Awake()
    {
        limit = GetComponent<BoxCollider2D>().size / 2;
        angle = GetComponent<BoxCollider2D>().attachedRigidbody.rotation;
        A = transform.TransformPoint(new Vector2(limit.x * 3 / 4, limit.y * 3 / 4));
        B = transform.TransformPoint(new Vector2(-limit.x * 3 / 4, -limit.y * 3 / 4));
        Debug.DrawLine(A, B, Color.red);
    }

    private void Update()
    {
        A = transform.TransformPoint(new Vector2(limit.x * 3 / 4, limit.y * 3 / 4));
        B = transform.TransformPoint(new Vector2(-limit.x * 3 / 4, -limit.y * 3 / 4));
        squishing = Physics2D.OverlapBox(transform.position, limit * 3 / 2, angle, LayerMask.GetMask("Enemy", "Default"), Mathf.Infinity, Mathf.Infinity);
        Debug.DrawLine(A, B, Color.red);
    }

    // Called upon every physics point.
    void FixedUpdate()
	{
        // Check to see if platforms are activated. If so, move platforms through the array.
        if (activated) 
        {
            // Check to see if the platform is not at the next point in the array.
            if (transform.position != movePoints[cur].position)
            {
                // If the platform is not at the cur position, move it towards that position.
                Vector2 p = Vector2.MoveTowards(transform.position, movePoints[cur].position, (speed * Time.deltaTime));

                // Creates vector used to get measurements for cosine.
                Vector2 temp = new Vector2((movePoints[cur].position.x - transform.position.x), (movePoints[cur].position.y - transform.position.y)); 
                cosine = temp.x / temp.magnitude;

                // Used by Player controller standing on it adjust relative speed for playing walking animation on platform.
                xspeed = speed * cosine; 
                GetComponent<Rigidbody2D>().MovePosition(p);
            }

            // If the platform reaches a point in the array, instance the next point.
            else
            {
                if (movePoints.Length == 1)
                {
                    activated = false;
                }
                else
                {
                    cur = (cur + 1) % movePoints.Length;
                }
            }
        }

        if (squishing)
        {
            Collider2D objsquished = Physics2D.OverlapArea(A, B, LayerMask.GetMask("Enemy", "Default"), Mathf.Infinity, Mathf.Infinity);
            ContactPoint2D[] results = new ContactPoint2D[6];
            objsquished.attachedRigidbody.GetContacts(results);
            Vector2 total = new Vector2(0, 0);
            foreach (ContactPoint2D f in results)
            {
                total += f.normal;
            }

            if (total.Equals(new Vector2(0, 0)))
            {
                if (results[0].normal.x == 0)
                {
                    total = new Vector2(Mathf.Sign(objsquished.attachedRigidbody.worldCenterOfMass.x - transform.position.x), 0);
                    Debug.Log("rigidbody x: " + objsquished.attachedRigidbody.worldCenterOfMass.x + ", platform x:" + transform.position.x);
                }
                else
                {
                    total = new Vector2(0, Mathf.Sign(objsquished.attachedRigidbody.worldCenterOfMass.y - transform.position.y));
                    Debug.Log("rigidbody y: " + objsquished.attachedRigidbody.worldCenterOfMass.y + ", platform y:" + transform.position.y);
                }

                Debug.Log("Total for vector being zero " + total);
                objsquished.attachedRigidbody.position += (total * 1f);
            }
            else
            {
                Debug.Log("Total for vector NOT being zero " + total);
                objsquished.attachedRigidbody.position += (total * 1f);
            }

            squishing = Physics2D.OverlapBox(transform.position, limit * 3 / 2, angle, LayerMask.GetMask("Enemy", "Default"), Mathf.Infinity, Mathf.Infinity);
        }
    }

    // A callable method from PlatformAddMovepointTrigger
    public void activatePlatforms() 
    {
        activated = true;
    }
}
