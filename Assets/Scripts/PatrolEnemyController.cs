﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PatrolEnemyController : MonoBehaviour 
{
	// An array to store transforms of movement points for the enemy.
    public Transform[] movePoints;

    // A boolean to set the enemy to move. May be called by a trigger.
    public bool activated = true;

    // A counter to iterate each point in the movePoints array.
    int cur = 0;

    // Speed of enemy movements.
    public float speed = 0.5f;

	// Boolean for flipping animations
	private bool facingRight = true;

	// Scale vector for flipping animation on movement
	private Vector3 theScale;

	// Called upon every physics point
	void FixedUpdate()
	{
		float h = transform.position.x - movePoints[cur].position.x;

        // Check to see if the enemy is not at the next point in the array.
        if (transform.position != movePoints[cur].position)
        {
            // If the enemy is not at the cur position, move it towards that position
            Vector2 p = Vector2.MoveTowards(transform.position, movePoints[cur].position, speed);
            GetComponent<Rigidbody2D>().MovePosition(p);
        }

        // If the enemy reaches a point in the array, instance the next point.
        else
        {
            cur = (cur + 1) % movePoints.Length;
        }

		// Logic for flipping animation
		if (h > 0 && !facingRight) 
        {
            Flip();
        } 
        else if (h < 0 && facingRight) 
        {
            Flip();
        }
	}

	// Flips animation to give illusion of movement.
	void Flip() 
    {
        facingRight = !facingRight;
        theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
