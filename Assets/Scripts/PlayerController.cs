﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    // Boolean to flip the player model depending on movement
    [HideInInspector] private bool facingRight = true;

    // Boolean to inhibit player movement for death and spawn animations
    private bool moveLock;

    // Booleans to instance a jump moment, depending on what kind of jump is available
    [HideInInspector] private bool jump1;
    [HideInInspector] private bool JumpDash;
    [HideInInspector] private bool wallJump;

    // Tells whether jumps are able
    private bool canWallJump;
    private bool jump1able;
    private bool canJumpDash;
    private bool wallJumpcontinue;

    // Boolean to check if the player is in position for a wall jump
    private bool inWallJumpPosition;
    private bool canCling; //boolean to determine ability to cling

    // An inspector field to check for Y axis speed of the character
    private float currentYSpeed;
    private float currentSpeed;

    //Audio Clips for jump actions
    public AudioClip jumpClip;
    public AudioClip jumpClip2;
    public AudioClip deathClip;
    public AudioClip levelWonClip;
    public AudioClip wallCling;
    public bool canPlayCling = true;
    public AudioClip damageTaken;
    //public AudioClip healthPickup;

    // Movement constraints
    public float moveForce = 400f;
    public float maxSpeed = 7f;
    public float maxYSpeed = 12f; //also affects max jump height by capping speed.
    public float jumpVelocity = 6.5f;
    private float jumpKillSpeed = 0.7f;
    public float speedAdjust = 0; //adusts speed when on moving surfaces for max speed calculations and animations
    private float relativeSpeed = 0; //variable to be used in animation and max speed calculations
    // Health
    public int maxHealth = 4;
    public float bufferTime = 2.8f;
    [HideInInspector] public int healthRemaining;

    [HideInInspector] public bool canBeHit = true;
    // Overlay color to change to when hurt and is invulnerable to damage
    private Color hurtColor = new Color(235, 0, 235);
    // Return color to normal when invulnerability ends
    private Color healthyColor = new Color(235, 235, 235);

    // Transforms to check for jump availability
    public Transform groundCheck;
    public Transform wallJumpCheck;

    // Scale is used as a variable here to both flip in one part, and to help with jump logic
    private Vector3 theScale;

    // Boolean to invoke a gravity puzzle
    public bool gravityPuzzle;

    // Boolean for HangGlider
    private bool hangGliding;
    private bool isHangGliding;
    private bool hangGlideStart;
    private float hangGlideTime;
    private bool canGlide;

    // Animator booleans for animations
    private bool grounded;
    public bool crouched;
    private bool moving;
    private bool dash;
    public bool death; //used in death trigger script

    // Instances of components of the character
    private Animator anim;
    private Rigidbody2D rb2d;
    private SpriteRenderer spriteRenderer;

    //stuff for raycasts for crouching
    public bool lefthit;
    public bool righthit;
    public Vector2 leftorigin;
    public Vector2 rightorigin;
    public float idleRayLength;
    public float originaloffset;
    public bool canStandUp;

    private Bounds bounds;// boundaries for determining positions of Raycasts
    private Vector2 touchOrigin = -Vector2.one;
    public Touchscreemovement touchscreemovement;

    //input holders
    private float horizontal;
    private float vertical;
    private bool jumphold;
    private bool jumppress;

    public GameObject Spawn;

	  // Use this for initialization
    void Awake() {
        //setting up boundaries of box collider for Raycast stuff
        //Time.timeScale = 0.01f;
        bounds = GetComponent<BoxCollider2D>().bounds;
        theScale = transform.localScale;
        Spawn = GameObject.FindGameObjectWithTag("Respawn");
        transform.position = Spawn.transform.position;
        Debug.Log("Gettng position now");

        //stuff for determining raycast
        idleRayLength = bounds.extents.y;
        originaloffset = GetComponent<BoxCollider2D>().offset.y;
        leftorigin = new Vector2((bounds.center.x - bounds.extents.x), bounds.center.y );
        rightorigin = new Vector2((bounds.center.x + bounds.extents.x), bounds.center.y );
        Vector2 leftend = new Vector2(leftorigin.x, leftorigin.y + (((2 * idleRayLength) - bounds.extents.y) * theScale.y));
        Vector2 rightend = new Vector2(rightorigin.x, rightorigin.y + (((2 * idleRayLength) - bounds.extents.y) * theScale.y));

        lefthit = Physics2D.Linecast(leftorigin, leftend, 1 << LayerMask.NameToLayer("Ground"));
        righthit = Physics2D.Linecast(rightorigin, rightend, 1 << LayerMask.NameToLayer("Ground"));

        //Debug.DrawLine(leftorigin, leftend, Color.red);
        //Debug.DrawLine(rightorigin, rightend, Color.red);

        // Instance the Rigidbody for the player character for adding movement forces
        rb2d = GetComponent<Rigidbody2D>();
        Vector2 A = new Vector2(transform.position.x + bounds.extents.x * 2/3, transform.position.y + (GetComponent<CircleCollider2D>().offset.y * theScale.y));
        Vector2 B = new Vector2(transform.position.x - bounds.extents.x * 2/3, transform.position.y + (GetComponent<CircleCollider2D>().offset.y - GetComponent<CircleCollider2D>().radius - .2f) * theScale.y);
        Debug.DrawLine(A, B, Color.red);

        grounded = Physics2D.OverlapArea(A, B, LayerMask.GetMask("Ground", "Default") , Mathf.Infinity, Mathf.Infinity);

        spriteRenderer = GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();
        healthRemaining = Spawn.GetComponent<SpawnPointScript>().getSpawnHealth();
        startMoveLock();

        touchscreemovement = GameObject.Find("TouchHolder").GetComponent<Touchscreemovement>();
    }

    void updateInputs()
    {
#if UNITY_IOS || UNITY_ANDROID
        horizontal = touchscreemovement.Horizontalaxis;
        if (Mathf.Abs(horizontal) > 1)
            horizontal = Mathf.Sign(horizontal); 

        vertical = touchscreemovement.Verticalaxis;
        if (Mathf.Abs(vertical) > 1)
            vertical = Mathf.Sign(vertical);

        jumppress = touchscreemovement.jumpPressed;
        jumphold = touchscreemovement.jumpHeld;
#else
        horizontal = Input.GetAxis("Horizontal");
        vertical = Input.GetAxis("Vertical");
        jumppress = Input.GetButtonDown("Jump");
        jumphold = Input.GetButton("Jump");
#endif
    }

    // Update is called once per frame
    void Update () 
    {
        bounds = GetComponent<BoxCollider2D>().bounds;
        leftorigin = new Vector2(bounds.center.x - bounds.extents.x, bounds.center.y);
        rightorigin = new Vector2(bounds.center.x + bounds.extents.x, bounds.center.y);
        Vector2 leftend = new Vector2(leftorigin.x, leftorigin.y + (((2 * idleRayLength) - bounds.extents.y) * theScale.y));
        Vector2 rightend = new Vector2(rightorigin.x, rightorigin.y + (((2 * idleRayLength) - bounds.extents.y) * theScale.y));

        lefthit = Physics2D.Linecast(leftorigin, leftend, 1 << LayerMask.NameToLayer("Ground"));
        righthit = Physics2D.Linecast(rightorigin, rightend, 1 << LayerMask.NameToLayer("Ground"));

        Vector2 A = new Vector2(transform.position.x + bounds.extents.x * 2/3, transform.position.y + (GetComponent<CircleCollider2D>().offset.y * theScale.y));
        Vector2 B = new Vector2(transform.position.x - bounds.extents.x * 2/3, transform.position.y + (GetComponent<CircleCollider2D>().offset.y - GetComponent<CircleCollider2D>().radius - .2f) * theScale.y);
        Debug.DrawLine(A, B, Color.red);

        grounded = Physics2D.OverlapArea(A, B, LayerMask.GetMask("Ground", "Slippery"), Mathf.Infinity, Mathf.Infinity);

        updateInputs();

        canStandUp = !lefthit && !righthit;

        // Setting both wall jump positions to tell if a wall jump is available
        inWallJumpPosition = Physics2D.Linecast(transform.position, wallJumpCheck.position, LayerMask.GetMask("Ground", "Slippery")) && !grounded;
        canCling = inWallJumpPosition && !Physics2D.Linecast(transform.position, wallJumpCheck.position, LayerMask.GetMask("Slippery"));

        Debug.DrawLine(transform.position, wallJumpCheck.position, Color.blue);

        // Reset and determine jump ability based on position.
        if (grounded) 
        {
            jump1able = true;
            canJumpDash = true;
            hangGlideTime = 0.0f;
            hangGlideStart = false;
            canGlide = true;
        }
        else if (canCling) 
        {
            jump1able = false;
            canWallJump = true;
            canJumpDash = true;
            hangGlideTime = 0.0f;
            hangGlideStart = false;
            canGlide = true;
        }
        else
        {
            jump1able = false;
            canWallJump = false;
        }

        if (jumppress && jump1able)
        {
            maxYSpeed = 20f;

            // If true a jump can occur.
            jump1 = true; 
        }

        // If the character has a wall jump available, is in a valid position, and hits the jump key, perform the appropriate jump.
        else if (jumppress && canWallJump)
        {
            wallJump = true;
        }
        else if (jumppress && canJumpDash)
        {
            maxSpeed = 7f;
            JumpDash = true;
            dash = true;
            Invoke("turnDashOff", .5f);
        }

        // Setting a boolean for the Animator to decide if the character is crouched.  Pressing the down key crouches the character
        else if (vertical <= -0.35)
        {
            crouched = true;
        }

        // Setting a boolean for the Animator to decide if the character is crouched.  Lifting up the down key stands the character up
        else if (vertical > -0.35)
        {
            if (canStandUp)
            {
                crouched = false;
            }
        }

        // Play wall cling sound
        if (inWallJumpPosition && canPlayCling)
        {
            GetComponent<AudioSource>().PlayOneShot(wallCling);
            canPlayCling = false;
        } 
        else if (!inWallJumpPosition)
        {
            canPlayCling = true;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        if (collision.gameObject.tag == "movingplatform")
        {
            PlatformMover movescript = collision.gameObject.GetComponent<PlatformMover>();

            if (Mathf.Abs(movescript.xspeed - speedAdjust) > 0.1)
            {
                Vector2 temp = new Vector2(speedAdjust - movescript.xspeed, 0f);
                rb2d.velocity -= temp;
                speedAdjust = movescript.xspeed;
            }
        }
        else
        {
            speedAdjust = 0;
        }
    }

  void OnCollisionExit2D(Collision2D collision)
  {
        if (!grounded)
        {
            speedAdjust = 0;
        }
  }

	void FixedUpdate()
	{
        // MOVEMENT HANDLING
        updateInputs();
        if (!moveLock)
        {
            if (!grounded)
            {
                speedAdjust = 0;
            }

        relativeSpeed = (rb2d.velocity.x - speedAdjust);

            // This line creates a variable to tell if the character is moving in the positive or negative direction.
            float h = 0;
            if (!wallJumpcontinue)
            {
                h = horizontal;
            }
#if UNITY_IOS || UNITY_ANDROID
            if (!wallJumpcontinue && h != 0)
                rb2d.velocity = new Vector2((h * maxSpeed) + speedAdjust, rb2d.velocity.y);
#else
            // If input for movement is input, and the character is under his max speed, apply appropriate velocity change
            // moveforce (400f)/ fixed update "framrate" (59.8802) = 6.68f change per frame 
            if (Mathf.Abs(h * relativeSpeed) <= maxSpeed)
           rb2d.velocity += (Vector2.right * h * 2);
#endif

            relativeSpeed = (rb2d.velocity.x - speedAdjust);

        // If input for movement is input, and the character is at or above max speed, taper the speed to max
        if (Mathf.Abs(relativeSpeed) > maxSpeed)
            {
                rb2d.velocity = new Vector2((Mathf.Sign(rb2d.velocity.x) * maxSpeed) + speedAdjust, rb2d.velocity.y);
            }

        // Cap the movement speed of upwards velocity.  This prevents stacking force on a jump
        if (Mathf.Abs(rb2d.velocity.y) > maxYSpeed)
            {
                rb2d.velocity = new Vector2(rb2d.velocity.x, Mathf.Sign(rb2d.velocity.y) * maxYSpeed);
            }

        // Test variable to check if capping Y velocity works
        currentYSpeed = rb2d.velocity.y;
        currentSpeed = rb2d.velocity.x;
        relativeSpeed = rb2d.velocity.x - speedAdjust;

            if (wallJumpcontinue && Mathf.Abs(relativeSpeed) < 2f)
            {
                wallJumpcontinue = false;
            }

        // For wall clinging.
        if (canCling)
        {
            GetComponent<PlatformEffector2D>().useSideFriction = true;
        }
        else
        {
            GetComponent<PlatformEffector2D>().useSideFriction = false;
        }

            // Actions if a Jump is Invoked.
            if (jump1) 
            {
                GetComponent<AudioSource>().PlayOneShot(jumpClip);
                float realjumpVelocity = 1.5f * jumpVelocity;

                //zero out force before jump is made so there is no force counteracting the force of the jump.
                rb2d.velocity = new Vector2(0f, 0f); 
                rb2d.velocity += new Vector2(0f, realjumpVelocity);
                jump1 = false;
                jump1able = false;
            }

            // Actions for a doubleJump instance.
            if (JumpDash) 
            {
                GetComponent<AudioSource>().PlayOneShot(jumpClip2);

                // Zero out force before jump is made so there is no force counteracting the force of the jump.
                rb2d.velocity = new Vector2(0f, 0f); 
                rb2d.velocity += new Vector2(0f, jumpVelocity * 1.5f);
                // Keep false to prvent triple jumping.
                JumpDash = false; 
                canJumpDash = false;
            }

            // Adds downward velocity whever the jump key isn't held allowing for variable jump heights.
            if (!jumphold && Vector2.Dot(rb2d.velocity, Vector2.up * theScale.y) > 0)
            {
                rb2d.velocity += Vector2.down * theScale * jumpKillSpeed;
            }

            // Artficial horizontal drag.
            if ((Mathf.Abs(horizontal) < 1 || wallJumpcontinue) && Mathf.Abs(relativeSpeed) > 0.1)
            {
                rb2d.velocity = new Vector2((relativeSpeed * 0.95f) + speedAdjust, rb2d.velocity.y);
            }

            // this prevents player from clinging and jumping at the same time...player cannot hold L/R axis while
            // in wall posititon and jump...prevents infinite wall climb and jumps straight up wall
            if (canCling && Mathf.Abs(horizontal) > 0)
            {
                if (Mathf.Abs(horizontal) >= 0.9)
                {
                    rb2d.gravityScale = 0;
                }
                else
                {
                    rb2d.gravityScale = 3 * theScale.y;
                }

                anim.SetTrigger("WallClimb");
            }
            else if (canCling && Mathf.Abs(horizontal) == 0)
            {
                rb2d.gravityScale = 3 * theScale.y;
            }

            // For when a moving platform appears under the player while wallclinging
            // made this specific so as to run when it doesn't need to and slow the game down, or interfere with gliding.
            else if (!canCling && rb2d.gravityScale == 0)
            {
                rb2d.gravityScale = 3 * theScale.y;
            }

            // Actions for a wallJump
            if (wallJump) 
            {
                GetComponent<AudioSource>().PlayOneShot(jumpClip);

                rb2d.velocity = new Vector2(0f, 0f);

                if (theScale.x > 0)
                {
                    rb2d.velocity += new Vector2(-13.36f, jumpVelocity);
                }
                else
                {
                    rb2d.velocity += new Vector2(13.36f, jumpVelocity);
                }

                wallJump = false;
                wallJumpcontinue = true;
            }

        if (isHangGliding && hangGlideStart)
            {
                hangGlideTime += Time.deltaTime;
            }
            

        if (hangGlideTime < 2.25f && (theScale.y * rb2d.velocity.y < 0))
            {
                canGlide = true;
            }
            else
            {
                canGlide = false;
            }

        hangGliding = vertical >= 0.65f && !grounded && !crouched && canGlide;

        if (hangGliding && !isHangGliding) 
        {
          rb2d.gravityScale = .3f * theScale.y;
          maxYSpeed = 3f;
          isHangGliding = true;
          hangGlideStart = true;
        }
        else if (!hangGliding && isHangGliding) 
        {
          rb2d.gravityScale = 3 * theScale.y;
          maxYSpeed = 20f;
          isHangGliding = false;
        }

        // Call the grounded function to reset jumps an animations if needed
        Grounded();

        // ANIMATION PARAMETER HANDLING

        // Flips the sprite and animations upon direction
        if (h > 0 && !facingRight)
        {
            FlipX();
        }
        else if (h < 0 && facingRight)
        {
            FlipX();
        }
            
        // A Y Flip call if the player interacts with a gravity puzzle switch
        if (gravityPuzzle)
        {
            FlipY();
        }
            
        // Setting booleans for the idle and walk animations
        if (relativeSpeed > -0.1 && relativeSpeed < 0.1)
        {
            moving = false;
        }
        else
        {
            moving = true;
        }
        }

        bool wallJumpAnim = inWallJumpPosition;

        // Setting all booleans for animations
        anim.SetBool("HangGlide", isHangGliding);
        anim.SetBool("WallClimb", wallJumpAnim);
        anim.SetBool("Moving", moving);
        anim.SetBool("Grounded", grounded);
        anim.SetBool("Crouching", crouched);
        anim.SetBool("Dash", dash);
        anim.SetBool("Death", death);
        currentYSpeed = rb2d.velocity.y;
    }

    // ANIMATION FLIP FUNCTIONS

    // Flipping the sprite and animations along the X axis for walking different directions
    void FlipX() 
    {
        facingRight = !facingRight;
        theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }

    // Flipping the sprite and physics for the gravity puzzle portions
    void FlipY() 
    {
      gravityPuzzle = false;
      rb2d.gravityScale *= -1;
      jumpVelocity *= -1;
      theScale = transform.localScale;
      theScale.y *= -1;
      transform.localScale = theScale;
    }

    // MISC FUNCTIONS

    // Code to reset jump abilities upon touching ground
    void Grounded() 
    {
        if (grounded == true) 
        {
            canWallJump = true;
            maxSpeed = 7f;
            maxYSpeed = 20f;
            dash = false;
        }
    }

    // Turn Dash off after initiating the animation
    void turnDashOff() 
    {
      dash = false;
    }

    // HEALTH FUNCTIONS

    // Add health fucntion in case the player bot picks up a health kit
    public void addHealth(int toAdd) 
    {
        if ((healthRemaining + toAdd) < maxHealth)
        {
            healthRemaining = healthRemaining + toAdd;
        }
        else 
        {
            healthRemaining = maxHealth;
        }
    }

    // Remove health or kill player if player takes damage from an enemy
    public void playerDamaged(int dmg) 
    {
        if (canBeHit) 
        {
            if (dmg != 999)
            {
                Spawn.GetComponent<ScoreCalculator>().addHit();
            }
            if (healthRemaining <= dmg) 
            {
            canBeHit = false;
            healthRemaining = 0;
            GetComponent<AudioSource>().PlayOneShot(deathClip);
            death = true;
            Spawn.GetComponent<ScoreCalculator>().addDeath();
            moveLock = true;
             Invoke("Respawn", 3f);
            }
            else
            {
                healthRemaining = healthRemaining - dmg;
                GetComponent<AudioSource>().PlayOneShot(damageTaken);
                this.GetComponent<SpriteBlink>().blink(bufferTime);
                startInvincibility();
            }
        }
    }

    // If the player bot collides with an enemy, this gives invulnerability seconds to get out of the area before damage is applied a second time
    void startInvincibility() 
    {
      canBeHit = false;
      spriteRenderer.color = hurtColor;
      Invoke("endInvincibility", bufferTime);
    }

    // A function to reset player color and their vulnerability
    void endInvincibility() 
    {
      spriteRenderer.color = healthyColor;
      canBeHit = true;
    }

    void startMoveLock() 
    {
      moveLock = true;
      Invoke("endMoveLock", 1f);
    }

    void endMoveLock() 
    {
      moveLock = false;
    }

    void Respawn() 
    {
      SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }

    // Prevents movement.
    public void EndLevelLock() 
    {
      moveLock = true;
      rb2d.velocity = new Vector2(0, 0);
        rb2d.gravityScale = 0;
    }

    public void OnTriggerEnter2D(Collider2D collider)
    {
        if (collider.gameObject.CompareTag("teleporter"))
        {
            Debug.Log("LevelWon");
            GameObject.FindGameObjectWithTag("CollectableText").GetComponent<CollectableimageManager>().SaveCollected();
            anim.SetBool("Off", true);
            canBeHit = false;
            EndLevelLock();
            StartCoroutine(EndLevel(collider.gameObject.GetComponent<LevelChangeTrigger>()));
        }
    }

    // This is a Coroutine, which can be paused at any point in it until certain parameters are fulfilled.
    // This includes other coroutines being completed which allows us to make sure it goes through step by step.
    IEnumerator EndLevel(LevelChangeTrigger tele)
    {
        GetComponent<AudioSource>().PlayOneShot(levelWonClip);
        yield return StartCoroutine(tele.animateTeleport());
        yield return StartCoroutine(Spawn.GetComponent<ScoreCalculator>().tallyPoints(healthRemaining, 1f));
        Destroy(Spawn);
        //Find way to delay this, make new coroutine
        yield return new WaitForSeconds(2);
        SceneManager.LoadScene(tele.selectedLevel);
    }
}
