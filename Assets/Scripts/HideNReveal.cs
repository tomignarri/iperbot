﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// For final level enemy.
public class HideNReveal : MonoBehaviour 
{
    // Objects to make invisible at start of level
    public GameObject[] hiding;

	// Use this for initialization
	void Start() 
    {
        foreach (GameObject p in hiding)
        {
            p.SetActive(false);
        }
	}

    // Make objects Visible again
	private void OnTriggerEnter2D(Collider2D collision)
	{
        foreach (GameObject p in hiding)
        {
            p.SetActive(true);
            if (p.GetComponent<EnemyFollowScript>() != null)
            {
                p.GetComponent<EnemyFollowScript>().ignoreCollider();
            }
        }
	}
}
