﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BulletMove : MonoBehaviour 
{
    // A reference to the player object for positioning and targeting by the bullet when instanced.
    private GameObject target;

    // Speed of the projectile.
    [SerializeField]
    private float moveSpeed = 5f;

    // Destroy projectile after a time
    private float terminationDelay = 5f;

    // Use this for initialization.
    private void Start() 
    {
        // Create the instance of the player object.
		target = GameObject.FindGameObjectWithTag("Player");

        // Create a Vector3 by finding the difference between the position of the player and bullet.
		Vector3 targetDir = target.transform.position - this.transform.position;

        // Finding the firing angle.
        // Atan is arctangent.
        float angle = Mathf.Atan2(targetDir.y, targetDir.x) * Mathf.Rad2Deg - 90f; 
        Quaternion q = Quaternion.AngleAxis(angle, Vector3.forward);
        this.transform.rotation = Quaternion.RotateTowards(this.transform.rotation, q, 180f);
    }

    // Update is called once per frame
    private void Update() 
    {
        transform.Translate(Vector3.up * Time.deltaTime * this.moveSpeed);
        Destroy(gameObject, this.terminationDelay);
    }

    // Deletes the projectile if it collides with a ground layer
    private void OnTriggerEnter2D(Collider2D collision) 
    {
        if (collision.gameObject.layer == 8) 
        {
            Destroy(gameObject);
        }
    }
}
