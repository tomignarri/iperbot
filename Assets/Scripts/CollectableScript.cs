﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableScript : MonoBehaviour {

	// A reference to the UI Text to update upon the collectable being collected
	private GameObject CollectableCount;

    public AudioClip healthPickup;

    // Use this for initialization
    void Start () {
		CollectableCount = GameObject.FindGameObjectWithTag("CollectableText");
        if (GameObject.FindGameObjectWithTag("Respawn").GetComponent<ScoreCalculator>().getCollectables().Contains(gameObject.name))
            Invoke("objectCollected", 1f);

        Physics2D.IgnoreCollision(GameObject.FindWithTag("Player").GetComponent<CircleCollider2D>(), GetComponent<BoxCollider2D>());

    }

    // Update is called once per frame
    void OnTriggerEnter2D(Collider2D collision)
    {

        if (collision.gameObject.CompareTag("Player"))
        {
            //call other object because this one will be destroyed very soon, along with it's audio source
            collision.gameObject.GetComponent<AudioSource>().PlayOneShot(healthPickup);
            objectCollected();
        }
    }

    void objectCollected()
    {
        // Calls a function in CollectableTextManager to add one to the count and update text
        CollectableCount.GetComponent<CollectableimageManager>().SetNewImage(gameObject.name);

        // Destroys the Game Object
        Destroy(gameObject);
    }

}