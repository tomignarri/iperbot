﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using UnityEngine.Networking;
using Random = UnityEngine.Random;

// also manages high score text component,
// can't put in the same place as the other UI managers because dontdestroyonLoad affects the whole heirarchy
public class TotalScoreHolder : MonoBehaviour 
{
    public Text ScoreDisplay;

    private int levelsCompleted = 0;
    private int totalTime = 0;
    private int totalCollectables = 0;
    private int totalDeaths = 0;
    private int totalHits = 0;

    private string code = string.Empty;

    // Use this for initialization
    void Awake()
    {
        DontDestroyOnLoad(this.gameObject);
        if (FindObjectsOfType(GetType()).Length > 1)
        {
            Debug.Log("Deleting extraneous copy");
            Destroy(gameObject);
        }
        else
        {
            // The += is (presumably) adding what methods need to be done when that even occurs.
            SceneManager.activeSceneChanged += refindText; 
        }
    }

    // For reseting the text element when it gets deleted upon the level being reloaded
    public void refindText(Scene Old, Scene New)
    {
        if (New.name == "MainMenu")
        {
            Destroy(gameObject);
        }
        else if (New.name == "GameEndScreen")
        {
            StartCoroutine(constructEndText());
        }
    }

    IEnumerator constructEndText()
    {
        ScoreDisplay = GameObject.FindWithTag("ScoreText").GetComponent<Text>();
        yield return StartCoroutine(GetDiscountCoupon());

        ScoreDisplay.text = "Congratulations! A winner is you! \n Levels Completed: " + levelsCompleted
        + "\n Time Taken: " + totalTime
        + " seconds \n Collectables collected: " + totalCollectables
        + " \n Times Hit: " + totalHits
        + " \n Times Killed " + totalDeaths;
        /*
        discount Code implimentation
              
        + " \n Below is your discount code for ShopIperDesign."
        + " \n Go to IperDesign.com/shopiperdesign to redeem: " 
        + " \n " + code;
        */
    }

    // Get discount coupon from the woocommerce csv file.
    IEnumerator GetDiscountCoupon()
    {
        UnityWebRequest www = UnityWebRequest.Get("http://dev.iperdesign.com/Iperbot/woocommerce-coupon.csv");
        int rnd = Random.Range(3, 1003);
        int i = 1;

        yield return www.SendWebRequest();

        if (www.isNetworkError || www.isHttpError)
        {
            Debug.Log(www.error);
            code = "Could not retrieve code, please try when connected to Internet";
        }
        else
        {
            // Show results as text
            code = "success";
            StringReader reader = new StringReader(www.downloadHandler.text);

            string line = reader.ReadLine();
            while (line != null && i < rnd)
            {
                line = reader.ReadLine();
                i++;
            }

            string[] tokens = line.Split(',');
            code = tokens[0];
            code = code.Substring(1, code.Length - 2);
        }

        yield return null;
    }

    public void countCollectables(int collectables)
    {
        totalCollectables += collectables;
    }

    public void countTime(int time)
    {
        totalTime += time;
    }

    public void countDeaths(int deaths)
    {
        totalDeaths += deaths;
    }

    public void addCompletion()
    {
        levelsCompleted += 1;
    }

    public void countHits(int hits)
    {
        totalHits += hits;
    }
}
