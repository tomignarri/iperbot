﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class directionalLights : MonoBehaviour 
{
    // State of directional light.
    public bool off; 
    private Animator anim;

    void Start() 
    {
        anim = GetComponent<Animator>();
    }

    // Player walks thriough directional light.
    void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            off = true;
            anim.SetBool("Off", off);
        }
    }
}
