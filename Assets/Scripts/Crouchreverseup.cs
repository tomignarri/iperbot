﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crouchreverseup : StateMachineBehaviour 
{
	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks.
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) 
    {
        animator.SetFloat("standheadstart", 1 - animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        if ((1 - animator.GetCurrentAnimatorStateInfo(0).normalizedTime) < 0)
        {
            animator.SetFloat("standheadstart", 0);
        }

        animator.SetFloat("crouchheadstart", animator.GetCurrentAnimatorStateInfo(0).normalizedTime);   
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
        {
            animator.SetFloat("crouchheadstart", 1);
        }

        if (animator.gameObject.GetComponent<PlayerController>().crouched == false)
        {
            animator.StopPlayback();
        }
    }
}
