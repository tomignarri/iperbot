﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CollectableimageManager : MonoBehaviour
{
    // A count for how many health remains
    private int count = 0;

    private GameObject CollectableCount;
    private List<string> names;
    public ScoreCalculator collectablessaver;

    public Image CollectableCounter;
    public Sprite Collectable4;
    public Sprite Collectable3;
    public Sprite Collectable2;
    public Sprite Collectable1;
    public Sprite Collectable0;

    // Use this for initialization
    void Start()
    {
        collectablessaver = GameObject.FindWithTag("Respawn").GetComponent<ScoreCalculator>();
        names = new List<string>(collectablessaver.getCollectables());

        // Start with the first count to text and set the text
        CollectableCounter.sprite = Collectable0;
        //max = count;
    }

    public void SetNewImage(string collected)
    {
        // Update the image to match the new count
        if (!names.Contains(collected))
        {
            names.Add(collected);
        }

        count += 1;
        switch (count)
        {
            default:
            case 4:
                CollectableCounter.sprite = Collectable4;
                break;
            case 3:
                CollectableCounter.sprite = Collectable3;
                break;
            case 2:
                CollectableCounter.sprite = Collectable2;
                break;
            case 1:
                CollectableCounter.sprite = Collectable1;
                break;
            case 0:
                CollectableCounter.sprite = Collectable0;
                break;
        }
    }

    public void SaveCollected()
    {
        collectablessaver.setSaved(names);
    }
}
