﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class HealthCounterTextManager : MonoBehaviour 
{
	// A count for how many health remains.
	private float count;
	private float max;
	private float percent;
	private GameObject player;

	public Text HealthText;

	// Use this for initialization.
	void Start() 
    {
		// Start with the first count to text and set the text.
		player = GameObject.FindGameObjectWithTag("Player");
		count = player.GetComponent<PlayerController>().healthRemaining;
		max = count;
		percent = (count / max * 100);
		HealthText.text = "Health: " + percent.ToString() + "%";
	}

	void Update() 
    {
        // Monitor health constantly to detect changes.
		count = player.GetComponent<PlayerController>().healthRemaining;
		percent = (count / max * 100);
		SetNewText();
	}

	public void SetNewText() 
    {
		// Update the text to match the new count.
		HealthText.text = "Health: " + percent.ToString() + "%";
	}
}
