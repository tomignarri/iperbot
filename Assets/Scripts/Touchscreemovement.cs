﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Touchscreemovement : MonoBehaviour 
{

    // Use this for initialization
    public float theta = 0;
    public Sprite dpadbacksprite;
    GameObject Cpad;
    GameObject Cstick;
    GameObject Jump;
    public GameObject Pause;

    private float controlScaling;

    // Nullable type, allows us to set value Types as null, does not make them stored as reference though.
    public Touch? padTouch = null; 
    public Touch? jumpTouch = null;
    public Touch? miscTouch1 = null;
    public Touch? miscTouch2 = null;

    // I don't knwo if a fingerId value canever be zero, so I'm taking a chance;
    public int padTouchID = -1;

    // private Vector2 padTouchorigin;  I wish the touch structure stored this by itself,
    public int jumpTouchID = -1;
    public int miscTouch1ID = -1;
    public int miscTouch2ID = -1;

    // The rate of change of the field of view in perspective mode.
    public float perspectiveZoomSpeed = 0.5f;       
    public float orthoZoomSpeed = 0.5f;

    GameObject Camera;
    public float Horizontalaxis;
    public float Verticalaxis;
    public bool jumpPressed;
    public bool jumpHeld;

    void Start () {
#if UNITY_IOS || UNITY_ANDROID
        Debug.Log(transform.parent.localScale.x);
        controlScaling = 1.7f / transform.parent.localScale.x;
        Pause.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1) * controlScaling;


        Camera = GameObject.FindGameObjectWithTag("MainCamera");
        Cpad = new GameObject("Cpad", typeof(RectTransform));
        Cpad.transform.SetParent(gameObject.transform);
        Cpad.GetComponent<RectTransform>().sizeDelta = new Vector2(220, 220);
        Cpad.GetComponent<RectTransform>().anchorMin = new Vector2(0, 0);
        Cpad.GetComponent<RectTransform>().anchorMax = new Vector2(0, 0);
        Debug.Log(Cpad.GetComponent<RectTransform>().lossyScale);

        Cpad.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1) * controlScaling;
        Cpad.GetComponent<RectTransform>().anchoredPosition = new Vector2(160, 146) * controlScaling;


        var backgroundpad = Cpad.AddComponent<Image>();
        backgroundpad.sprite = dpadbacksprite;
        backgroundpad.color = new Color(0.6039216f, 0.7843138f, 0.3176471f, 0.5f);
        var Area = Cpad.AddComponent<CircleCollider2D>();
        Area.radius = 68f;
        Area.isTrigger = true;

        Jump = new GameObject("Jump Button", typeof(RectTransform));
        Jump.transform.SetParent(gameObject.transform);
        Jump.GetComponent<RectTransform>().sizeDelta = new Vector2(220, 220);
        Jump.GetComponent<RectTransform>().anchorMax = new Vector2(1, 0);
        Jump.GetComponent<RectTransform>().anchorMin = new Vector2(1, 0);

        Jump.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1) * controlScaling;
        Jump.GetComponent<RectTransform>().anchoredPosition = new Vector2(-160, 146) * controlScaling;

        var Jumpimage = Jump.AddComponent<Image>();
        Jumpimage.sprite = dpadbacksprite;
        Jumpimage.color = new Color(0.1176471f, 0.2980392f, 0.2156863f, 0.75f);
        var Area2 = Jump.AddComponent<CircleCollider2D>();
        Area2.radius = 80;
        Area2.isTrigger = true;

        Cstick = Instantiate(Cpad, Cpad.transform);
        Cstick.name = "Cstick";
        Cstick.GetComponent<RectTransform>().anchorMin = new Vector2(0.5f, 0.5f);
        Cstick.GetComponent<RectTransform>().anchorMax = new Vector2(0.5f, 0.5f);
        Cstick.GetComponent<RectTransform>().sizeDelta = new Vector2(100, 100);
        Cstick.GetComponent<RectTransform>().anchoredPosition = new Vector2(0, 0);
        Cstick.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1);
        Cstick.GetComponent<Image>().color = new Color(0.1176471f, 0.2980392f, 0.2156863f, 0.75f);
        Destroy(Cstick.GetComponent<CircleCollider2D>());
#else
        Destroy(gameObject);

#endif
    }
	
    // Update is called once per frame
	void Update() 
    {
#if UNITY_IOS || UNITY_ANDROID

        // a touch is made
        if (Input.touchCount > 0)
        {
            Touch[] AllTouches = new Touch[Input.touchCount];
            Debug.Log(Input.touchCount + " Touches");

            // add touches to touch array
            for (int i = 0; i < Input.touchCount; i++)
                AllTouches[i] = Input.touches[i];

            // search for particular touch
            if (padTouch == null)
                padTouch = getButtonTouch(AllTouches, Cpad, ref padTouchID);
            if (jumpTouch == null)
                jumpTouch = getButtonTouch(AllTouches, Jump, ref jumpTouchID);

            //search for miscelacnous touches    
            for (int i = 0; i < AllTouches.Length; i++)
            {
                if (AllTouches[i].fingerId != padTouchID && AllTouches[i].fingerId != jumpTouchID)
                {
                    if (miscTouch1 == null)
                    {
                        miscTouch1 = AllTouches[i];
                        miscTouch1ID = miscTouch1.Value.fingerId;
                    }

                    else if (miscTouch1 != null && miscTouch2 == null)
                    {
                        miscTouch2 = AllTouches[i];
                        miscTouch2ID = miscTouch2.Value.fingerId;
                    }
                }
            }

            if ((Camera.GetComponent<Camera>() != null) && miscTouch1 != null && miscTouch2 != null)
            {
                // Store both touches.
                miscTouch1 = getTouchbyIndex(AllTouches, miscTouch1ID);
                miscTouch2 = getTouchbyIndex(AllTouches, miscTouch2ID);
                if ((miscTouch1.Value.phase == TouchPhase.Ended || miscTouch1.Value.phase == TouchPhase.Canceled) || (miscTouch2.Value.phase == TouchPhase.Ended || miscTouch2.Value.phase == TouchPhase.Canceled))
                {
                    miscTouch1 = null;
                    miscTouch1ID = -1;
                    miscTouch2 = null;
                    miscTouch2ID = -1;
                }

                else
                {
                    // Find the position in the previous frame of each touch.
                    Vector2 touchZeroPrevPos = miscTouch1.Value.position - miscTouch1.Value.deltaPosition;
                    Vector2 touchOnePrevPos = miscTouch2.Value.position - miscTouch2.Value.deltaPosition;

                    // Find the magnitude of the vector (the distance) between the touches in each frame.
                    float prevTouchDeltaMag = (touchZeroPrevPos - touchOnePrevPos).magnitude;
                    float touchDeltaMag = (miscTouch1.Value.position - miscTouch2.Value.position).magnitude;

                    // Find the difference in the distances between each frame.
                    float deltaMagnitudeDiff = prevTouchDeltaMag - touchDeltaMag;

                    // If the camera is orthographic...
                    if (Camera.GetComponent<Camera>().orthographic)
                    {
                        // ... change the orthographic size based on the change in distance between the touches.
                        Camera.GetComponent<Camera>().orthographicSize += deltaMagnitudeDiff * orthoZoomSpeed;
                        // dont zoom any closer than this
                        Camera.GetComponent<Camera>().orthographicSize = Mathf.Max(Camera.GetComponent<Camera>().orthographicSize, 8f);

                        Camera.GetComponent<Camera>().orthographicSize = Mathf.Min(Camera.GetComponent<Camera>().orthographicSize, 20f);
                    }
                }
            }


            if (padTouch != null)
            {
                padTouch = getTouchbyIndex(AllTouches, padTouchID);
                //Debug.Log(padTouch.Value.phase);
                if (padTouch.Value.phase == TouchPhase.Ended || padTouch.Value.phase == TouchPhase.Canceled)
                {
                    Cstick.transform.localPosition = new Vector2(0,0);
                    padTouch = null;
                    padTouchID = -1;
                }

                else
                {
                    if (Cpad.GetComponent<CircleCollider2D>().OverlapPoint(padTouch.Value.position))
                    {
                        //Debug.Log("On "+padTouch.Value.position);
                        Cstick.transform.position = padTouch.Value.position;
                    }
                    else
                    {
                        //Debug.Log("Off " + padTouch.Value.position);
                        Vector2 place = padTouch.Value.position - (Vector2)Cpad.transform.position;
                        place /= place.magnitude;
                        place *= 84;
                        Cstick.transform.localPosition = place;
                        theta = 0;
                    }
                }

            }

            if (jumpTouch != null)
            {
                jumpTouch = getTouchbyIndex(AllTouches, jumpTouchID);
                //Debug.Log(padTouch.Value.phase);

                // pressing the jump button
                if (jumpTouch.Value.phase == TouchPhase.Began)
                {
                    Debug.Log("Jump phase began does");
                    jumpPressed = true;
                    jumpHeld = true;
                }

                // letting go of the jump button
                else if (jumpTouch.Value.phase == TouchPhase.Ended || jumpTouch.Value.phase == TouchPhase.Canceled)
                {
                    jumpPressed = false;
                    jumpHeld = false;
                    jumpTouch = null;
                    jumpTouchID = -1;
                }


                else
                {
                    jumpPressed = false;

                    if (Jump.GetComponent<CircleCollider2D>().OverlapPoint(jumpTouch.Value.position))
                    {
                        jumpHeld = true;
                        //Jump.GetComponent<Image>().color = new Color(0, 0.2588235f, 0.7921569f, 0.6078432f);
                    }

                    else
                    {
                        jumpHeld = false;
                        jumpTouch = null;
                        jumpTouchID = -1;
                    }
                }

            }

            //Material testing for angle
            /*
            if (Cstick.transform.localPosition.x == 0)
            { 
                theta = 0; 
            }

            else
            {
                theta = Mathf.Atan2(Cstick.transform.localPosition.y, Cstick.transform.localPosition.x);
                theta *= 360 * 0.5f / 3.14159265359f;
            }*/

            Horizontalaxis = Mathf.Round(Cstick.transform.localPosition.x / .60f) / 100f;
            Verticalaxis = Mathf.Round(Cstick.transform.localPosition.y / .60f) / 100f;
        }
#endif
    }

    // For Some reason this isn't a default function, and I have a feeling that I am going to need it.
    Touch? getTouchbyIndex(Touch[] touches, int ID) 
    {
        foreach (Touch f in touches)
        {
            if (f.fingerId == ID)
            {
                return f;
            }
        }

        return null;
    }

    Touch? getButtonTouch(Touch[] touches, GameObject button, ref int ID)
    {
        foreach (Touch f in touches)
        {
            if (button.GetComponent<CircleCollider2D>().OverlapPoint(f.position))
            {
                Debug.Log("There is a touch on " + button.name);
                ID = f.fingerId;
                Debug.Log(f.fingerId);
                return f;
            }
        }

        return null;
    }
}
