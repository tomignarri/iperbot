﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpriteBlink : MonoBehaviour 
{
	public float spriteBlinkingTimer = 0.0f;
	public float spriteBlinkingMiniDuration = 0.1f;
	public float spriteBlinkingTotalTimer = 0.0f;
    private float bufferTime = 0.0f;
	[HideInInspector] public bool startBlinking = false;
	[HideInInspector] public bool blinking = false;

    void Update()
    {
        if (startBlinking == true)
        {
            SpriteBlinkingEffect();
        }
    }
  
    public void blink(float buffer)
    {
		startBlinking = true;
        bufferTime = buffer;
	}

    private void SpriteBlinkingEffect()
    {
        blinking = true;
        spriteBlinkingTotalTimer += Time.deltaTime;

        if (spriteBlinkingTotalTimer >= bufferTime)
        {
            startBlinking = false;
            spriteBlinkingTotalTimer = 0.0f;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = true;
            blinking = false;
            return;
        }

		spriteBlinkingTimer += Time.deltaTime;

        if (spriteBlinkingTimer >= spriteBlinkingMiniDuration)
        {
            spriteBlinkingTimer = 0.0f;
            this.gameObject.GetComponent<SpriteRenderer>().enabled = !this.gameObject.GetComponent<SpriteRenderer>().enabled;
        }
    }
}
