﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class zoomOutCamera : MonoBehaviour 
{
    private float zoomSpeed = 0.5f;
    public float targetOrtho;
    public float smoothSpeed = 2.0f;
    public float minOrtho = 8.0f;
    public float maxOrtho = 20.0f;
    private float zoom;

    // Use this for initialization
    void Start() 
    {
        targetOrtho = Camera.main.orthographicSize;
    }

    // Update is called once per frame
    void Update() 
    {
        float scroll;
#if UNITY_IOS || UNITY_ANDROID
        scroll = zoom;
#else
        scroll = Input.GetAxis("Zoom");
#endif 
        if (scroll != 0.0f)
        {
            targetOrtho -= Mathf.Sign(scroll) * zoomSpeed;

            // "clamp" target ortho wherever it's value is between min and max.
            targetOrtho = Mathf.Clamp(targetOrtho, minOrtho, maxOrtho); 
        }

        Camera.main.orthographicSize = Mathf.MoveTowards(Camera.main.orthographicSize, targetOrtho, smoothSpeed * Time.deltaTime);
        targetOrtho = Camera.main.orthographicSize;
    }

    public void ZoomIn()
    {
        zoom = 1;
    }

    public void ZoomOut()
    {
        zoom = -1;
    }
}
