﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointScript : MonoBehaviour 
{
    private GameObject Spawn;
    private SpriteRenderer image;
    private bool activated;
    private Animator anim;

    // Use this for initialization
    private void Start() 
    {
        Spawn = GameObject.FindGameObjectWithTag("Respawn");
        image = gameObject.GetComponent<SpriteRenderer>();
        anim = GetComponent<Animator>();

        if (Spawn.transform == gameObject.transform)
        {
            activated = true;
            image.color = Color.red;
        }
        else
        {
            activated = false;
            image.color = Color.white;
        }
    }

    // Update is called once per frame
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (!activated && collision.gameObject.CompareTag("Player"))
        {
            // Call other object because this one will be destroyed very soon, along with it's audio source.
            GetComponent<AudioSource>().Play();
            GameObject.FindGameObjectWithTag("CollectableText").GetComponent<CollectableimageManager>().SaveCollected();

            // Change spawn to position of checkpoint.
            Spawn.transform.position = gameObject.transform.position; 

            int health = collision.gameObject.GetComponent<PlayerController>().healthRemaining;

            if (health <= 1)
            {
                collision.gameObject.GetComponent<PlayerController>().healthRemaining = 2;
                Spawn.GetComponent<SpawnPointScript>().setSpawnHealth(2);
            }
            else
            {
                Spawn.GetComponent<SpawnPointScript>().setSpawnHealth(health);
            }
                
            activated = true;
            anim.SetBool("Activated", activated);
        }
    }
}
