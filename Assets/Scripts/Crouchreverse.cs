﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Crouchreverse : StateMachineBehaviour {

	 // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
	override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        //animator.SetFloat("standheadstart", 0);
        //animator.SetFloat("crouchheadstart", (1 - animator.GetCurrentAnimatorStateInfo(0).normalizedTime));
        if (animator.gameObject.GetComponent<PlayerController>().lefthit || animator.gameObject.GetComponent<PlayerController>().righthit)
        {
            animator.gameObject.GetComponent<PlayerController>().crouched = true;
        }
    }

	// OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
	override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
        if (animator.gameObject.GetComponent<PlayerController>().lefthit || animator.gameObject.GetComponent<PlayerController>().righthit)
        {
            animator.gameObject.GetComponent<PlayerController>().crouched = true;
            //animator.StopPlayback();
        }
        animator.SetFloat("standheadstart", animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime > 1)
            animator.SetFloat("standheadstart", 1);

        animator.SetFloat("crouchheadstart", (1 - animator.GetCurrentAnimatorStateInfo(0).normalizedTime));
        if (animator.GetCurrentAnimatorStateInfo(0).normalizedTime < 0)
            animator.SetFloat("crouchheadstart", 0);

        if (animator.gameObject.GetComponent<PlayerController>().crouched == true)
        {
            //Debug.Log(animator.GetCurrentAnimatorStateInfo(0).normalizedTime);
            animator.StopPlayback();
        }

    }

	// OnStateExit is called when a transition ends and the state machine finishes evaluating this state
	//override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateMove is called right after Animator.OnAnimatorMove(). Code that processes and affects root motion should be implemented here
	//override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}

	// OnStateIK is called right after Animator.OnAnimatorIK(). Code that sets up animation IK (inverse kinematics) should be implemented here.
	//override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {
	//
	//}
}
