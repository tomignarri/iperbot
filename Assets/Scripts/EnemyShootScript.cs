﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Timers;

public class EnemyShootScript : MonoBehaviour 
{
    // Reference to the prefab Bullet.
    public GameObject Bullet;

    // Range of the turret in units.
    public float range = 30f;
   
    // Time between bullet fires when inside turret range.
    public float shootDelay = 3.0f;
    private float lastTimeShot = float.MinValue;

    public Color rayColor;
    public AudioClip enemyShoot;
    public bool enemyInSight;

    private GameObject player;
    Transform playerPosWhenShoot;

    // Use this for initialization.
    void Start() 
    {
        //Create an instance of the Player for positioning.
        player = GameObject.FindGameObjectWithTag("Player");
    }

	// Update is called once per frame
    void Update() 
    {
        // Creating an easy to reference position for measuring range
        playerPosWhenShoot = player.transform; 

        // Find distance between Player and Turret, to compare to range
        float distance = Vector3.Distance(playerPosWhenShoot.position, transform.position);

        // If the distance is within the range, and the time of delay has passed, fire a bullet.
        if ((distance < range) && Time.time > lastTimeShot + shootDelay && enemyInSight) 
        {
            fireBullet();
            lastTimeShot = Time.time;
            GetComponent<AudioSource>().PlayOneShot(enemyShoot);
        }
    }

    // detect if player is in range
    private void OnTriggerStay2D(Collider2D col)
    {
        if (col.gameObject.CompareTag("Player"))
        {
            enemyInSight = true;
        }
    }
   
    private void OnTriggerExit2D(Collider2D col)
    {
            enemyInSight = false;
    }

    // Called to instance a bullet and fire at the player
    void fireBullet() 
    {
        // Create an instance of the bullet sprite.  The Bullet has a script attached for movement towards the player
        Instantiate(Bullet, transform.position, Quaternion.identity);
    }
}
