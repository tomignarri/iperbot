﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// Behavior for Lever.
public class PlatformAddMovepointTrigger : MonoBehaviour 
{
    // Array of platforms to activate.
    public GameObject[] platsToActivate;

    // Boolean to test if the switch has been activated.  If so, the sprite will rotate.
    public bool switchedOn = false;

    public bool animationFinished = false;

    public AudioClip pullSwitch;

    private Animator anim;

	void Start()
	{
        anim = GetComponent<Animator>();
	}

	void OnTriggerEnter2D(Collider2D collision)
	{
        // If the trigger is activated, begin to move the platforms to their desired points.
        if (collision.gameObject.CompareTag("Player")) 
        {
            if (!switchedOn)
            {
                GetComponent<AudioSource>().PlayOneShot(pullSwitch);
            }

            foreach (GameObject g in platsToActivate) 
            {
                g.GetComponent<PlatformMover>().activatePlatforms();
            }

            // I really dislike how this script and activate platforms are together, but it's too embedded to change.
            switchedOn = true;

            anim.SetBool("switchedOn", switchedOn);
        }
	}
}
