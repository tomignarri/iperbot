﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFollowScript : MonoBehaviour
{
    // Variables to control the enemy object.
    private float moveForce = 20f;
    public float maxSpeed = 3f;

    // An instance of the attached Rigidbody to apply force to.
    private Rigidbody2D rb2d;

    // Range for the enemy to begin to follow.
    public float startDistanceFollow = 10f;

    // Instances of the Player for reference when applying range
    GameObject Player;
    Transform currentPlayerTransform;
    GameObject currentEnemy;

    // For Flipping animation on direction.
    private Animator anim;
    public bool facingLeft = true;
    private Vector3 theScale;
    private bool inRange = false;


    public AudioSource playerSpotted;
    public bool playEnemySpotted;
    public bool playEnemySpottedToggle;
    public float distancetotarget;
    public float testspeed;
    public float impulse = 0;

    // For initialization.
    void Start()
    {
        // Instance the Player object to the playable character.
        Player = GameObject.FindGameObjectWithTag("Player");
        theScale = transform.localScale;

        // Instance the Rigidbody for force application
        rb2d = GetComponent<Rigidbody2D>();
        anim = GetComponent<Animator>();

        playerSpotted = GetComponent<AudioSource>();
        playEnemySpotted = true;
        ignoreCollider();
    }

    // For some reason it wouldn't reach this part in time before hideNreveal turned it off.
    public void ignoreCollider()
    {
        Physics2D.IgnoreCollision(Player.GetComponent<BoxCollider2D>(), GetComponents<BoxCollider2D>()[1]);
        Physics2D.IgnoreCollision(Player.GetComponent<CircleCollider2D>(), GetComponents<BoxCollider2D>()[1]);
    }

    void FixedUpdate()
    {
        // Create a copy of the player position for easy reference.
        currentPlayerTransform = Player.transform;
        distancetotarget = Mathf.Abs((currentPlayerTransform.position.x - transform.position.x));

        // If the player is within the range to allow the enemy to start following, begin the follow script.
        if (Vector2.Distance(currentPlayerTransform.position, transform.position) < startDistanceFollow && !Physics2D.Raycast(transform.position, Vector2.left * theScale.x, distancetotarget, LayerMask.GetMask("Ground", "Slippery")))
        {
            // Don't do anything if player and enemy are on top of one another.
            if (distancetotarget > 0.1)
            {
                // If the player is to the right of the enemy, move towards the player.
                if ((currentPlayerTransform.position.x - transform.position.x) > 0)
                {
                    rb2d.AddForce(Vector2.right * 1 * moveForce);
                }
                // If the player is to the left, move the enemy towards the player.
                else
                {
                    rb2d.AddForce(Vector2.right * -1 * moveForce);
                }
            }

            inRange = true;

            // Play audio source if in range.
            if (playEnemySpotted && playEnemySpottedToggle)
            {
                playerSpotted.Play();

                // This bool prevents repeated Play() call.
                playEnemySpottedToggle = false;
            }

            distancetotarget = Mathf.Abs((currentPlayerTransform.position.x - transform.position.x));
        }
        else
        {
            inRange = false;
        }

        testspeed = rb2d.velocity.x;

        // Cap the speed of the enemy for fairness
        if (Mathf.Abs(rb2d.velocity.x) > maxSpeed)
        {
            rb2d.velocity = new Vector2(Mathf.Sign(rb2d.velocity.x) * maxSpeed, rb2d.velocity.y);
        }

        testspeed = rb2d.velocity.x;

        // To flip animation
        if ((currentPlayerTransform.position.x - transform.position.x) > 0 && facingLeft)
        {
            Flip();
        }
        else if ((currentPlayerTransform.position.x - transform.position.x) < 0 && !facingLeft)
        {
            Flip();
        }

        // Stop audio source when out of range.
        if (!inRange)
        {
            playEnemySpottedToggle = true;
            playerSpotted.Stop();
        }

        anim.SetBool("PlayerInRange", inRange);
    }

    // Flip function to flip the sprite to give the illusion of the enemy following
    void Flip()
    {
        facingLeft = !facingLeft;
        theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}